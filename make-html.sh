#!/usr/bin/env sh

set -o errexit
set -o nounset

mkdir --parents public/
truncate -s 0 public/index.html

cat >> public/index.html <<HTML
<!DOCTYPE html>
<html>
  <head>
    <title>What does LGTM stand for?</title>
  </head>
  <body>
    <h1>What does LGTM stand for?</h1>
    <ul>
HTML

sort < lgtm.yml | sed 's|^- \(.*\)$|      <li>\1</li>|' >> public/index.html

cat >> public/index.html <<HTML
    </ul>
    <a href="https://gitlab.com/winniehell/lgtm/edit/master/lgtm.yml">Add your own definition</a>
  </body>
</html>
HTML
